window.addEventListener("load", () => {
    window.vue = new Vue({
        el: "#app",
        data: {
            title: "Watch List",
            watch_later: [],
            movielist: [],
            movies: [],
            isLoading: true,
            isShow: false,
            name: ""
        },

        created() {

            // fetch("movies.json")
            //     .then((res) => {
            //         return res.json()
            //     })
            //     .then((res) => {
            //         this.watch_later = res.watch_later;
            //         this.movielist = res.movielist;

            //     })


        },

        methods: {

            fetchData() {
                this.isShow = true;
                this.isLoading = true;
                this.movies = {};

                this.$http.get('https://api.themoviedb.org/3/search/movie?api_key=4df48a82d59cef5af895702c99141a1e&query=' + this.name)
                    .then((res) => {
                        return res.json()
                    })
                    .then((res) => {

                        this.isLoading = false;

                        this.movies = res.results;
                    })

            },


            addWatchList(index) {
                const myMovie = this.movies.splice(index, 1);
                this.watch_later.push(myMovie[0])
            },
        },



    });
})